var gadget;
var projectKey;

var atlassianBaseUrl;
var gadget;


function startGadget(baseUrl) {
    atlassianBaseUrl = baseUrl;
    initGadget();

}


function displayInGadget(args){
    var content = Infusion.SubtasksStatus.Templates.chartBox({project: args});
    gadget.getView().html(content);

    gadgets.window.adjustHeight();
}

function displayContentInGadget(content){
    gadget.getView().html(content);

    gadgets.window.adjustHeight();
}

function initGadget() {
                gadget = AJS.Gadget({
                    baseUrl: atlassianBaseUrl,
                    useOauth: "/rest/gadget/1.0/currentUser",
                    view: {
                        template: function(args) {
                            var gadget = this;

                            projectKey = gadget.getPref("jiraProject");
                            var sprintsNo = gadget.getPref("sprintsNo");
                            var allSprints;



            /*                AJS.$.ajax({
                                url: "/rest/api/2/field",
                                type: "GET",
                                dataType: "json",
                                async:false,
                                contentType: "application/json",
                                success:
                                    function (fields) {
                                        var romField=_.find(fields, function(field){return field.name =="ROM Estimate"})

                                    }
                                    });*/


                            AJS.$.ajax({
                                url: "/rest/greenhopper/1.0/rapidviews/list?projectKey="+projectKey,
                                type: "GET",
                                dataType: "json",
                                contentType: "application/json",
                                success:
                                    function (views) {
                                        var viewId = views.views[0].id;
                                        AJS.$.ajax({
                                            url: "/rest/greenhopper/1.0/sprintquery/"+viewId+"?includeHistoricSprints=false&includeFutureSprints=true",
                                            type: "GET",
                                            dataType: "json",
                                            contentType: "application/json",
                                            success:
                                                function (argSprints) {
                                                    var notClosedSprints = _.filter(argSprints.sprints,function(sprint){return sprint.state != 'CLOSED';})
                                                    var ids =_.first(notClosedSprints,sprintsNo).map(function(sprint){return sprint.id});
                                                    var sprints = AJS.$("<div/>");
                                                    var results = [];
                                                    for(i=0; i<sprintsNo;i++){
                                                        var sp = notClosedSprints[i];
                                                        if(sp === undefined)
                                                            continue;
                                                        var id = sp.id;
                                                        var name = sp.name;
                                                        //var name = sp.name +' ('+sp.state+')';
                                                        var result = {
                                                            sprintId: id,
                                                            sprintName: name,
                                                            };
                                                        sprints.append(AJS.$("<b/>").text(name))
                                                        AJS.$.ajax({
                                                            url: "/rest/greenhopper/1.0/rapid/charts/sprintreport?rapidViewId="+argSprints.rapidViewId+"&sprintId="+id,
                                                            type: "GET",
                                                            async:false,
                                                            dataType: "json",
                                                            contentType: "application/json",
                                                            success:
                                                                function (args) {
                                                                    result.startDate = args.sprint.startDate;
                                                                    result.endDate = args.sprint.endDate;
                                                                    result.state= args.sprint.state;
                                                                    result.allIssuesEstimateSum= args.contents.allIssuesEstimateSum.value ? args.contents.allIssuesEstimateSum.value : 0;
                                                                    result.completedIssuesEstimateSum= args.contents.completedIssuesEstimateSum.value ? args.contents.completedIssuesEstimateSum.value : 0;
                                                                    result.issuesNotCompletedEstimateSum= args.contents.issuesNotCompletedEstimateSum.value ? args.contents.issuesNotCompletedEstimateSum.value : 0;
                                                                                                                                        sprints.append(" start date: "+args.sprint.startDate+" end date: "+args.sprint.endDate)
                                                                }

                                                        })
                                                        //Get list of all subtasks
                                                        AJS.$.ajax({
                                                            url: "/rest/api/2/search?jql=project%20%3D%20"+projectKey+"%20and%20Sprint%20%3D%20"+id+"%20and%20type%20%3D%20Sub-task%20ORDER%20BY%20key%20asc&fields=assignee%2Cprogress%2Cproject%2Cissuetype%2Cstatus%2Caggregatetimeoriginalestimate%2Caggregatetimeestimate%2Caggregatetimespent",
                                                            type: "GET",
                                                            async:false,
                                                            dataType: "json",
                                                            contentType: "application/json",
                                                            success:
                                                                function (arg) {
                                                                    //var onlyAssigned = _.filter(arg.issues,function(issue){ return issue.fields.assignee != undefined; } );
                                                                    var onlyAssigned = arg.issues;
                                                                    var allsubtasks =_.map(onlyAssigned,function(issue){
                                                                                        return {
                                                                                            assigned: issue.fields.assignee == undefined ? 'Unassigned' : issue.fields.assignee.displayName,
                                                                                            orgTime: issue.fields.aggregatetimeoriginalestimate,
                                                                                            remaining: issue.fields.aggregatetimeestimate,
                                                                                            total: issue.fields.progress.total,
                                                                                            logged: issue.fields.aggregatetimespent,
                                                                                            avatarUrl: issue.fields.assignee == undefined ? '/secure/useravatar?size=medium&avatarId=10123' : issue.fields.assignee.avatarUrls['32x32']
                                                                                            };
                                                                        });
                                                                    var subtasks=_.groupBy(allsubtasks,function(subtask){return subtask.assigned;})
                                                                    var grouped = _.map(subtasks, function(a){
                                                                        return _.reduce(a,function(memo,fold){
                                                                            return {
                                                                                assigned: memo.assigned,
                                                                                orgTime: memo.orgTime + fold.orgTime,
                                                                                remaining: memo.remaining + fold.remaining,
                                                                                total: memo.total + fold.total,
                                                                                logged: memo.logged + fold.logged,
                                                                                avatarUrl: memo.avatarUrl
                                                                                };
                                                                            });
                                                                        })

                                                                    var userList = AJS.$("<ul/>");
                                                                    AJS.$(grouped).each(function(group) {
                                                                        userList.append(
                                                                            AJS.$("<li/>").text(this.assigned +" estimated time: "+this.orgTime/3600 + " remainging: "+this.remaining/3600))

                                                                    });
                                                                    result.userStats= grouped;
                                                                    results[i] = result;
                                                                    sprints.append(userList);


                                                                }
                                                        });
                                                    }

                                                    var displayInfo ={projectKey: projectKey, sprints: results};
                                                    AJS.$.ajax({
                                                        url: "/rest/api/2/project/"+projectKey,
                                                        type: "GET",
                                                        async:false,
                                                        dataType: "json",
                                                        contentType: "application/json",
                                                        success:
                                                            function (args) {
                                                                displayInfo.projectName = args.name;
                                                            }
                                                            });
                                                    displayInGadget(displayInfo);

                                                },
                                             error: function(args){
                                                 console.log('Error while retrieving sprints for rapidview: ',args.responseText);
                                                 displayContentInGadget(AJS.$("<div/>").text('Error while retrieving sprints for rapidview: ' + args.responseText));

                                             }
                                        });
                                    },
                                error: function(args){
                                    console.log('Error while retrieving rapidviews for project, args: ',args.responseText);
                                    displayContentInGadget(AJS.$("<div/>").text('Error while retrieving rapidviews for project, args: '+args.responseText));

                                }
                            });



                        }
                        ,
                        args: [{
//                            key: "projectData",
//                            ajaxOptions: function() {
//                                return {
//                                    type: "GET",
//                                    dataType: "json",
//                                    contentType: "application/json",
//                                };
//                            }
                        }]
                    },
                    config: {
                        descriptor: function(args){
                                return {
                                    fields:[
                                        {
                                            userpref: "jiraProject",
                                            type: "string",
                                            value: "",
                                            label: "Project key: "
                                        },
                                        {
                                            userpref: "sprintsNo",
                                            type: "int",
                                            value: "",
                                            label: "Number of sprints: "
                                        },
                                        AJS.gadget.fields.nowConfigured()]
                                        }
                        }
                    }
                });
            }